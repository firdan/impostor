""" default settings for impostor """
from django.conf import settings

ALLOW_IMPOSTOR_BY_EMAIL = getattr(settings,
                                  'ALLOW_IMPOSTOR_BY_EMAIL',
                                  True)

IMPOSTOR_GROUP = getattr(settings,
                         'IMPOSTOR_GROUP',
                         None)

