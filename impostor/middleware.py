""" this is simple way to track session """
from impostor import IMPOSTOR_SESSION_KEY, IMPOSTOR_HIJACK_KEY, IMPOSTOR_TOKEN
from impostor.utils import logit
from .models import ImpostorLog

from django.contrib.auth import get_user_model
import datetime
from tracking.models import Visitor


User = get_user_model()

class ImpostorMiddleware(object):

    def process_response(self, request, response):
        # remove any previous
        request.session.pop(IMPOSTOR_HIJACK_KEY, None)
        # check session
        if not hasattr(request, 'session') :
            return response

        # get current user in session
        user = getattr(request, 'user', None)

        # check for anonymous, don't do anything
        if user is None or user.is_anonymous() :
            return response

        # check whether requested as normal user or hijacked user
        # when IMPOSTOR_SESSION_KEY show as None then we can assume that
        # the one requested is normal user
        admin = None
        impostor_key = request.session.get(IMPOSTOR_SESSION_KEY, None)
        try :
            admin = User.objects.get(pk=impostor_key)
        except User.DoesNotExist :
            pass
        
        # when request.user is hijacked user
        if admin and user:
            
            # save the impostor log
            logit(admin=admin,
                  user=user,
                  request=request,
                  session_token_key=IMPOSTOR_TOKEN)

        # when user is normal user
        else :
            
            # looking whether has record for someone who is hijacking this user
            logs = ImpostorLog.objects.filter(imposted_as=user.id,
                                              logged_out__isnull=True)
            if logs.count() > 0:
                # TODO : need to solve when multiple user hijack the same user
                # assume the most top is the impostor user
                impostor_id = logs[0].impostor.id

                # check impostor user still has the session or not
                # using tracking app
                visitor = Visitor.objects.filter(user=User.objects.get(pk=impostor_id)).order_by('-start_time')[:1]
                visitor = visitor[0]

                if not (visitor.session_ended() or visitor.session_expired()):
                    request.session[IMPOSTOR_HIJACK_KEY] = impostor_id
                    request.session.save()

        return response


