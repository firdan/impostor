from django.test.testcases import TestCase
from django.test.client import RequestFactory
from django.contrib.sessions.middleware import SessionMiddleware
from django.contrib.auth import BACKEND_SESSION_KEY, SESSION_KEY

from impostor.tests import dummies
from impostor.middleware import ImpostorMiddleware
from impostor.models import ImpostorLog
from impostor import IMPOSTOR_SESSION_KEY, IMPOSTOR_HIJACK_KEY, IMPOSTOR_TOKEN
from tracking.models import Visitor
from impostor.utils import logit


class TestImpostorMiddleware(TestCase):

    def test_as_normal_user(self):
        user = dummies.create_user('dummy')
        request = RequestFactory().request()
        response = 'dummy'
        SessionMiddleware().process_request(request)
        # assign user
        request.user = user

        request.session[BACKEND_SESSION_KEY] = 'dummy'
        request.session[SESSION_KEY] = user.pk
        request.session.save()

        middleware = ImpostorMiddleware()
        middleware.process_response(request, response)

        self.assertEqual(ImpostorLog.objects.count(), 0)

    def test_imposting_as_someone(self):
        admin = dummies.create_user('admin')
        user = dummies.create_user('dummy')

        request = RequestFactory().request()
        response = 'dummy'
        SessionMiddleware().process_request(request)

        # assign user
        request.user = user

        # assign session
        request.session[BACKEND_SESSION_KEY] = 'dummy'
        request.session[SESSION_KEY] = user.pk
        request.session[IMPOSTOR_SESSION_KEY] = admin.pk
        request.session.save()

        middleware = ImpostorMiddleware()
        middleware.process_response(request, response)

        self.assertEqual(ImpostorLog.objects.count(), 1)

    def test_someone_imposting(self):
        admin = dummies.create_user('admin')
        user = dummies.create_user('dummy')

        request = RequestFactory().request()
        response = 'dummy'
        SessionMiddleware().process_request(request)

        # assign user
        request.user = user

        # assign session
        request.session[BACKEND_SESSION_KEY] = 'dummy'
        request.session[SESSION_KEY] = user.pk
        request.session.save()

        # create visitor dummy
        logit(admin=admin,
             user=user,
             request=request,
             session_token_key=IMPOSTOR_TOKEN)
        Visitor.objects.create(pk=request.session.session_key, user=admin)

        middleware = ImpostorMiddleware()
        middleware.process_response(request, response)

        self.assertEqual(ImpostorLog.objects.count(), 1)
        self.assertEqual(request.session[IMPOSTOR_HIJACK_KEY], admin.pk)

