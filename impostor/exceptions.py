

class AuthBackendMismatch(Exception):
    """
    auth backend different with current user backend
    """
    def __str__(self):
        return repr('are you sure auth backend session and user is the same ?')


class AuthBackendNotDetected(Exception):
    """cannot detect user backend"""
    def __str__(self):
        return repr("are you sure user has login ? auth_backend session cannot be detected")
