""" 
generate dummies user and models to fill the database
useful for testing purposes
"""
import random
import datetime
import string

from django.contrib.webdesign import lorem_ipsum
from django.contrib.auth import get_user_model
from django.db import IntegrityError, transaction


User = get_user_model()

def create_random_string(total_character, feed=None):
    """ create random string using string.printable
    
    :param total_character: how long the string will be
    :type total_character: integer
    
    """
    feed = string.ascii_letters + "_" if feed is None else feed
    words = ""
    i = 0
    while i < total_character:
        words += feed[random.randrange(0, len(feed) - 1)]
        i += 1
    return words


def create_random_phone():
    return create_random_string(10, string.digits)


def create_user (user):
    """ 
    create user , if user is already exist, then find a new unique 
    username, all users used a "test" as password
    """
    try :
        with transaction.atomic():
            return User.objects.create_user(username=user, password="test")
    except IntegrityError :
        # add new unique characters
        user += create_random_string(random.randint(1, 5))
        create_user(user)


def create_users(seed):
    for i in range(seed):
        create_user(lorem_ipsum.words(1, False))


def date_random():
    seed_year = random.randint(1988, 1995)
    seed_day = random.randint(1, 28)
    seed_month = random.randint(1, 12)
    date_random = datetime.date(
                                year=seed_year,
                                day=seed_day,
                                month=seed_month)
    return date_random
