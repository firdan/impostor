import os
from setuptools import setup, find_packages

README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='impostor2',
    version='0.9.4',
    packages=find_packages(exclude=['demo', 'demo.*']),
    include_package_data=True,
    license='MIT License',
    description='User can login as a different user.',
    long_description=README,
    url='http://tpxventures.com/',
    author='Firdan Machda',
    author_email='firdan.machda@tapestrix.net',
    download_url='https://bitbucket.org/tapestrix/impostor2/',
    install_requires=[
        'django-tracking2',
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
