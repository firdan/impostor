""" test when user imposting as another user """
from django.test.testcases import TestCase
from django.contrib.auth import get_user_model, BACKEND_SESSION_KEY, SESSION_KEY
from django.test.client import RequestFactory
from django.contrib.sessions.middleware import SessionMiddleware
from django.contrib.auth.models import Group

from impostor.tests import dummies
from impostor.models import AllowedGroup, ImpostorOption
from impostor import imposting, stop_imposting, IMPOSTOR_SESSION_KEY, \
    IMPOSTOR_TOKEN

User = get_user_model()
class TestImposting(TestCase):

    def setUp(self):
        self.admin = dummies.create_user('admin')
        self.admin.is_superuser = True
        self.admin.save()

        self.impostor_user = dummies.create_user('impostor')
        self.user1 = dummies.create_user('user1')
        self.user2 = dummies.create_user('user2')
        self.user_not_allow = dummies.create_user('not_allow')
        self.user_allow = dummies.create_user('allow')

        not_allow = ImpostorOption.objects.create(user=self.user_not_allow)
        not_allow.allow = False
        not_allow.save()

        ImpostorOption.objects.create(user=self.user_allow)

        # groups
        impostor_group = Group.objects.create(name='impostor')
        normal_group = Group.objects.create(name='normal')
        impostor_group.user_set.add(self.impostor_user)
        normal_group.user_set.add(self.user1)

        AllowedGroup.objects.create(group=impostor_group)

    def create_request(self):
        request = RequestFactory().request()
        SessionMiddleware().process_request(request)
        return request

    def imposting_test(self, impostor, user, allowed=True):
        """ 
        if allowed is true then test should produce when
        user is allowed to pretend another user 
        """
        request = self.create_request()
        # assign user
        request.user = impostor

        request.session[BACKEND_SESSION_KEY] = 'dummy'
        request.session[SESSION_KEY] = impostor.pk
        request.session.save()

        # start imposting
        imposting(request, user)
        self.assertEqual((request.user == user), allowed,
                         'imposting error, is this correct: %s , %s is %s ?' % \
                         (user, request.user, allowed))
        self.assertEqual((request.session[SESSION_KEY] == user.pk), allowed)

        try :
            request.session[IMPOSTOR_SESSION_KEY]
            self.assertTrue(allowed, 'cannot get impostor session key')
        except KeyError :
            self.assertTrue(not allowed, 'cannot get impostor session key')

        stop_imposting(request)
        self.assertEqual((request.user == impostor), True,
                         'cannot stop imposting: %s != %s' % (request.user, impostor))
        self.assertEqual((request.session[SESSION_KEY] == impostor.pk), True)

        try :
            request.session[IMPOSTOR_SESSION_KEY]
            request.session[IMPOSTOR_TOKEN]
            self.assertTrue(False, 'cannot get impostor session key')
        except KeyError :
            self.assertTrue(True, 'cannot get impostor session key')

    def test_imposting_as_another_user(self):
        self.imposting_test(self.admin, self.user1, allowed=True)

    def test_not_impostor_imposting_as_another_user(self):
        # from normal group
        self.imposting_test(self.user1, self.user2, allowed=False)
        # not from group
        self.imposting_test(self.user2, self.user1, allowed=False)

    def test_imposting_as_anonym(self):
        anonym = User.objects.get(pk= -1)
        self.imposting_test(anonym, self.user1, allowed=False)
        self.imposting_test(anonym, self.impostor_user, allowed=False)
        self.imposting_test(self.admin, anonym, allowed=False)
        self.imposting_test(self.impostor_user, anonym, allowed=False)

    def test_user_from_group(self):
        # impostor user is from group impostor that allowed to do imposting
        self.imposting_test(self.impostor_user, self.user1, allowed=True)

    def test_allowed_user(self):
        self.imposting_test(self.admin, self.user_not_allow, allowed=False)
        self.imposting_test(self.admin, self.user_allow, allowed=True)
