from impostor.models import ImpostorLog


def get_ip(request):
    """ get user ip address """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def logit(admin, user, ip=None, request=None, session_token_key=None):
    """ 
    log when an impostor pretend as another user 
    :params admin: an User model for user who will be an impostor
    :params user: an User model for user who will be hijacked by admin
    :params ip: ip address of impostor, if ip is none , it will try to get
                user ip
    :params session_token_key: a variable to keep token session key 
    
    """

    ip = ''
    token = None

    # if request detected get impostor token
    if request:
        token = request.session.get(session_token_key, None)
        # if ip not provided try to get ip from request
        if not ip:
            ip = get_ip(request)

    try :
        log_entry = ImpostorLog.objects.get(token=token)
    except ImpostorLog.DoesNotExist:
        log_entry = ImpostorLog.objects.create(impostor=admin,
                                               imposted_as=user,
                                               impostor_ip=ip,
                                              )
        if request :
            log_entry.session_key = request.session.session_key
            log_entry.save()

    # assign session token for impostor
    if request and log_entry.token and session_token_key:
        request.session[session_token_key] = log_entry.token

    return log_entry


def module_exists(module_name):
    try:
        __import__(module_name)
    except ImportError:
        return False
    else:
        return True
