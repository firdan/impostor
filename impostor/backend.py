import inspect
import django.core.validators

from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from django.http import HttpRequest
from models import AllowedGroup
from .settings import ALLOW_IMPOSTOR_BY_EMAIL, IMPOSTOR_GROUP
from impostor.utils import logit

try:
	group = Group.objects.get(name=IMPOSTOR_GROUP)
except:
	group = None

def is_allowed(user):
	'''
	get group for current user to hijack another user
	params user: User model
	'''

	if user and user.pk > 0 :
		groups = user.groups.all()
		allowed_groups = AllowedGroup.objects.filter(group__in=groups)

		if len(allowed_groups) > 0:
			return True
	return False


def find_request():
	'''
	Inspect running environment for request object. There should be one,
	but don't rely on it.
	'''
	frame = inspect.currentframe()
	request = None
	f = frame

	while not request and f:
		if 'request' in f.f_locals and isinstance(f.f_locals['request'], HttpRequest):
			request = f.f_locals['request']
		f = f.f_back

	del frame
	return request


class AuthBackend(object):
	supports_anonymous_user = False
	supports_object_permissions = False
	supports_inactive_user = False

	def check_user(self, identification):
		User = get_user_model()
		try:
			django.core.validators.validate_email(identification)
			if ALLOW_IMPOSTOR_BY_EMAIL :
				try :
					return User.objects.get(email__iexact=identification)
				except User.DoesNotExist:
					return None
			else :
				return None
		except django.core.validators.ValidationError:
			try :
				return User.objects.get(username__iexact=identification)
			except User.DoesNotExist:
				return None


	def authenticate(self, username=None, password=None):
		auth_user = None

		try:
			# Admin logging as user?
			admin, uuser = [ uname.strip() for uname in username.split(" as ") ]

			# Check if admin exists and authenticates
			admin_user = self.check_user(admin)

			# check user has the power to pretend as anoter user or not
			if (admin_user.is_superuser or is_allowed(admin_user) \
			    ) and admin_user.check_password(password):
				auth_user = self.check_user(uuser)

			if auth_user:
				# Superusers can only be impersonated by other superusers
				if auth_user.is_superuser and not admin_user.is_superuser:
					auth_user = None
					raise Exception("Superuser can only be impersonated by a superuser.")
				# Try to find request object and maybe be lucky enough to find IP address there
				request = find_request()
				logit(admin_user, auth_user, request=request)
		except:  # Nope. Do nothing and let other backends handle it.
			pass
		return auth_user

	def get_user(self, user_id):
		try:
			return get_user_model().objects.get(pk=user_id)
		except get_user_model().DoesNotExist:
			return None
