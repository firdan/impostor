from django import template
from django.contrib.auth import get_user_model

from impostor import IMPOSTOR_SESSION_KEY, IMPOSTOR_HIJACK_KEY
from impostor.utils import get_ip, module_exists


register = template.Library()
User = get_user_model()

def check_imposed(request):
    try :
        impostor = User.objects.get(pk=request.session.get(IMPOSTOR_SESSION_KEY))
    except User.DoesNotExist:
        impostor = None
    return impostor


@register.inclusion_tag('impostor/show_info_tag.html', takes_context=True)
def show_impostor_info(context):
    request = context['request']
    impostor = check_imposed(request)
    ip = get_ip(request)
    return dict(impostor=impostor,
                ip=ip,
                user=request.user)


@register.inclusion_tag('impostor/hijack_warning_tag.html', takes_context=True)
def show_hijack_warning(context):
    request = context['request']
    key = request.session.get(IMPOSTOR_HIJACK_KEY, None)
    if key >= 0:
        return dict(result=True)
    return dict(result=False)


@register.assignment_tag(takes_context=True)
def status_imposed(context):
    """ 
    get user object who being imposed as other user if exist or None 
    
    how to use (in template):

        {% status_imposed as impostor %} # need to define it first
        {% impostor %}          # this has become object, use it as usual 
        {% impostor.username %} # example or 
        {% if impostor %}       # something like this
        wow
        {% endif }
    """
    request = context['request']
    impostor = check_imposed(request)
    return impostor


@register.assignment_tag(takes_context=True)
def status_hijack(context):
    """ 
    get user object who imposed as other current user if exist or None 
    
    how to use (in template):

        {% status_hijack as hijack %} # need to define it first
        {% hijack %}          # this has become object, use it as usual 
        {% hijack.username %} # example or 
        {% if hijack %}       # something like this
        wow
        {% endif }
    """
    request = context['request']
    key = request.session.get(IMPOSTOR_HIJACK_KEY, None)
    if key >= 0:
        try:
            return User.objects.get(pk=key)
        except User.DoesNotExist :
            pass
    return False