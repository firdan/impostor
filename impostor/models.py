import impostor
import datetime
from django.db import models
from django.conf import settings
# from django.contrib.auth.signals import user_logged_in, user_logged_outs
import hashlib, time
from django.contrib import auth
from django.contrib.auth.signals import user_logged_out
from django.dispatch import receiver

User = None
if settings.AUTH_USER_MODEL:
	User = settings.AUTH_USER_MODEL
else :
	User = auth.models.User

class ImpostorLog(models.Model):
	impostor = models.ForeignKey(User, related_name='impostor', db_index=True)
	imposted_as = models.ForeignKey(User, related_name='imposted_as', verbose_name='Logged in as', db_index=True)
	impostor_ip = models.IPAddressField(verbose_name="Impostor's IP address", null=True, blank=True)
	logged_in = models.DateTimeField(auto_now_add=True, verbose_name='Logged on')
	logged_out = models.DateTimeField(null=True, blank=True)
	token = models.CharField(max_length=32, blank=True, db_index=True, unique=True)

	session_key = models.CharField(max_length=40, null=True)

	def save(self, *args, **kwargs):
		if not self.token and self.impostor:
			self.token = hashlib.sha1(self.impostor.username + str(time.time())).hexdigest()[:32]
		super(ImpostorLog, self).save(*args, **kwargs)


class AllowedGroup(models.Model):
	""" Alowed Group of users to have impostor power """
	group = models.OneToOneField(auth.models.Group)
	added_date = models.DateTimeField(auto_now_add=True)


class ImpostorOption(models.Model):
	""" Option to either allow or not, to be hijacked """
	user = models.ForeignKey(settings.AUTH_USER_MODEL)
	allow = models.BooleanField(default=True)


@receiver(user_logged_out)
def remove_session_callback(sender,**kwargs):
	impostor.stop_imposting(kwargs.get('request'))
	user = kwargs.get('user')
	# assume that if user log out with any unifinished imposed session 
	# is finished by setting logged_out is now
	# TODO : try only with the same session by using request.session ?
	ImpostorLog.objects.filter(impostor=user.id,
                               logged_out__isnull=True,
                               ).update(logged_out=datetime.date.today())
