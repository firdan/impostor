import django.core.validators
import datetime
from django.test import TestCase
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.models import Group
from django.conf import settings as default_settings

from impostor import settings
from impostor.models import ImpostorLog
from impostor.forms import BigAuthenticationForm


admin_username = 'real_test_admin'
admin_email = 'admin@mail.com'
admin_pass = 'admin_pass'
user_username = 'real_test_user'
user_email = 'real@mail.com'
user_pass = 'user_pass'
user2_username = 'user2'
user2_email = 'user2@mail.com'

# test only when user use authentication backend
requirements = None
try :
	requirements = 'impostor.backend.AuthBackend' in default_settings.AUTHENTICATION_BACKENDS
except AttributeError:
	requirements = False

class TestImpostorLogin(TestCase):
	def setUp(self):
		real_admin = get_user_model().objects.create(username=admin_username,
													email=admin_email,
													password=admin_pass)
		real_admin.is_superuser = True
		real_admin.set_password(admin_pass)
		real_admin.save()

		real_user = get_user_model().objects.create(username=user_username,
													email=user_email,
													password=user_pass)
		real_user.set_password(user_pass)
		real_user.save()

		user = get_user_model().objects.create(username=user2_username,
											email=user2_email)
		user.set_password(user_pass)
		user.save()

		# groups
		impostor_group = Group.objects.create(name='impostor')
		normal_group = Group.objects.create(name='normal')


	def test_login_user(self):
		u = authenticate(username=user_username, password=user_pass)
		real_user = get_user_model().objects.get(username=user_username)

		self.failUnlessEqual(u, real_user)

	def test_login_user_with_email(self):
		""" impostor should not handle this, should do as default """
		u = authenticate(email=user_email, password=user_pass)
		real_user = get_user_model().objects.get(email=user_email)

		self.failIfEqual(u, real_user)

	def test_login_admin(self):
		u = authenticate(username=admin_username, password=admin_pass)
		real_admin = get_user_model().objects.get(username=admin_username)

		self.failUnlessEqual(u, real_admin)

	def test_login_admin_with_email(self):
		""" impostor should not handle this, should do as default """
		u = authenticate(username=admin_email, password=admin_pass)
		real_admin = get_user_model().objects.get(username=admin_username)

		self.failIfEqual(u, real_admin)

	def as_user_model(self, identification):
		""" change to user model based on email or username """
		try:
			django.core.validators.validate_email(identification)
			try:
				return get_user_model().objects.get(email=identification)
			except get_user_model().DoesNotExist:
				return None
		except django.core.validators.ValidationError :
			try :
				return get_user_model().objects.get(username=identification)
			except get_user_model().DoesNotExist:
				return None

	def common_test_login_as_user(self, admin_identity, admin_pass, user_identity):
		if requirements :
			# bypass if settings not allowed by email

			if not settings.ALLOW_IMPOSTOR_BY_EMAIL :
				try:
					django.core.validators.validate_email(admin_identity)
					return True  # exit
				except django.core.validators.ValidationError :
					pass
				try:
					django.core.validators.validate_email(user_identity)
					return True  # exit
				except django.core.validators.ValidationError :
					pass

			no_logs_entries = len(ImpostorLog.objects.all())
			self.failUnlessEqual(no_logs_entries, 0, 'log entry still exist')

			u = authenticate(username="%s as %s" % (admin_identity, user_identity),
							password=admin_pass)

			real_user = self.as_user_model(user_identity)
			admin_user = self.as_user_model(admin_identity)

			self.failUnlessEqual(u, real_user, 'failed to authenticate %s' % real_user)

			# Check if logs contain an entry now
			logs_entries = ImpostorLog.objects.all()

			# bypass if settings not allowed by email
			if not settings.ALLOW_IMPOSTOR_BY_EMAIL :
				self.failUnlessEqual(len(logs_entries), 1, 'log entry is %s' % len(logs_entries))
				entry = logs_entries[0]
				today = datetime.date.today()
				lin = entry.logged_in
				self.failUnlessEqual(entry.impostor, admin_user, 'impostor is not %s' % admin_user)
				self.failUnlessEqual(entry.imposted_as, real_user, 'failed imposted as %s' % real_user)
				self.assertTrue(lin.year == today.year and lin.month == today.month and lin.day == today.day)
				self.assertTrue(entry.token and entry.token.strip() != "")

	def test_login_admin_as_user_email_username(self):
		self.common_test_login_as_user(admin_email, admin_pass, user_username)

	def test_login_admin_as_user_email_email(self):
		self.common_test_login_as_user(admin_email, admin_pass, user_email)

	def test_login_admin_as_user_username_username(self):
		self.common_test_login_as_user(admin_username, admin_pass, user_username)

	def test_login_admin_as_user_username_email(self):
		self.common_test_login_as_user(admin_username, admin_pass, user_email)

	def common_test_failed_login_as_user(self, user_identity1, user_identity1_pass, user_identity2):
		# bypass if settings not allowed by email
		if not settings.ALLOW_IMPOSTOR_BY_EMAIL :
			try:
				django.core.validators.validate_email(user_identity1)
				return True  # exit
			except django.core.validators.ValidationError :
				pass
			try:
				django.core.validators.validate_email(user_identity2)
				return True  # exit
			except django.core.validators.ValidationError :
				pass

		no_logs_entries = len(ImpostorLog.objects.all())
		self.failUnlessEqual(no_logs_entries, 0)

		u = authenticate(username="%s as %s" % (user_identity1, user_identity2),
						password=admin_pass)

		user2 = self.as_user_model(user_identity2)

		self.failIfEqual(u, user2)

		# Check if logs contain an entry now
		logs_entries = ImpostorLog.objects.all()
		self.failUnlessEqual(len(logs_entries), 0)

	def test_user_cannot_login_as_another_user(self):
		self.common_test_failed_login_as_user(user_username, user_pass, user2_username)
		self.common_test_failed_login_as_user(user_username, user_pass, user2_email)
		self.common_test_failed_login_as_user(user_email, user_pass, user2_username)
		self.common_test_failed_login_as_user(user_email, user_pass, user2_email)

	def test_user_cannot_login_as_admin(self):
		self.common_test_failed_login_as_user(user_username, user_pass, admin_username)
		self.common_test_failed_login_as_user(user_username, user_pass, admin_email)
		self.common_test_failed_login_as_user(user_email, user_pass, admin_username)
		self.common_test_failed_login_as_user(user_email, user_pass, admin_email)

	def test_form(self):
		if requirements :
			initial = { 'username': user_username, 'password': user_pass}
			form = BigAuthenticationForm(data=initial)
			self.assertTrue(form.is_valid())
			self.failUnlessEqual(form.cleaned_data['username'], user_username)
			self.failUnlessEqual(form.cleaned_data['password'], user_pass)

			new_uname = "%s as %s" % (admin_username, user_username)  # Longer than contrib.auth default of 30 chars
			initial = { 'username': new_uname, 'password': admin_pass }
			form = BigAuthenticationForm(data=initial)
			self.assertTrue(form.is_valid())
			self.failUnlessEqual(form.cleaned_data['username'], new_uname)
			self.failUnlessEqual(form.cleaned_data['password'], admin_pass)

			del initial['password']
			form = BigAuthenticationForm(data=initial)
			self.assertFalse(form.is_valid())
