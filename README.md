#Impostor

This application is based on impostor created by Marko Samastur with link https://github.com/samastur/Impostor/. But, during development it doesn't support Django 1.6 and custom user also some feature that needed for development are missing, thus a little changes are required.

##Feature
new feature:

 * tag to show when user imposing as another user
 * tag warning when an impostor impose a user
 * start imposing a user without login
 * stop imposing a user without logout
 * imposing user with email (assuming email exist)

##Installation
First install django-tracking2(`pip install django-tracking2` or `easy_install django-tracking2`) then impostor app files as you would any other Django app (e.g. ``pip install django-imposing``). then add `impostor` app to `INSTALLED_APPS`

	INSTALLED_APPS = (
		...,
		'tracking',  # from django-tracking2
		'impostor',
		'...'
	)

###Impose with login
This is an optional, if you want to have abilities to impose user with login then some changes to your Django settings file are in order. To AUTHENTICATION_BACKENDS add `impostor.backend.AuthBackend`

This will add impostor auth backend to other backends. AUTHENTICATION_BACKENDS
is a tuple listing backends and if you don't have it yet, then add following
lines to your settings:

    AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend',
        'impostor.backend.AuthBackend',
    )

###Set up templatetags
add `DJANGO.CORE.CONTEXT_PROCESSORS.REQUEST` to 
`TEMPLATE_CONTEXT_PROCESSORS` to enable template tags to show status of user like so

	TEMPLATE_CONTEXT_PROCESSORS = (
		...,
		`django.core.context_processors.request`,
	)

and middleware to show who is currently impose user, this feature need django-tracking2 to be installed, add `impostor.middleware.ImpostorMiddleware` add the bottom. 
	
	MIDDLEWARE_CLASSES = (
		'tracking.middleware.VisitorTrackingMiddleware', # from django tracking
		...,
		'impostor.middleware.ImpostorMiddleware',
	)
	

last, Run `python manage.py syncdb --all` to create needed table and you are set.


##Usage

###Login as another user
By now you should have a working system. This means that your superuser users
(users with is_superuser flag set to True) can log in as different user by
using their password and following concatenation:

    ``staff_username as users_username``

Example: Let's say my username is markos and I want to login as user fry.
Then I would use 'markos as fry' as my username and my normal password for
password.

Every such log in is logged in ImpostorLog table that can be seen through
Django admin interface, but for obvious security reasons can't be
manipulated there.

You can widen set of users who can impose as other users by adding a group to AllowedGroup model then add the user to that group. Users belonging to a group with this name will also be able to pretend to be somebody else (but not superusers).

Impostor also provides a replacement authentication form, because two
usernames can easily exceed 30 character limit of original form. Its name
is BigAuthenticationForm and you can find it in impostor.forms.

NOTE: Only superuser users can use this (you have to turn on is_superuser
for every user that needs this privilege) or those belonging to
AllowedGroup and every such log in gets recorded.

Also use AllowedGroup cautiously because it still allows impersonating
somebody with different set of permissions (and hence security breach).

###imposting and stop_impoting
To impose a user use `imposting(request,user_object)` and to stop impose a user `stop_imposting(request)`. 

example somewhere inside `appname/views.py`

	from django.shortcuts import render_to_response
	from django.contrib.auth import get_user_model
	from django.template.context import RequestContext

	from impostor import imposting, stop_imposting


	User = get_user_model()


	def some_view1(request, username=None):
	    if username :
	        user = User.objects.get(username=username)
	        imposting(request, user)
	    return render_to_response('app_name/template_view.html',
	                              context_instance=RequestContext(request))


	def some_view2(request):
	    stop_imposting(request)
	    return render_to_response('app_name/template_view.html',
	                              context_instance=RequestContext(request))

NOTE: this feature only work when user not impose another user with a login function, if user impose using a login then she/he need to logout to stop impose the user.

###refusal
User can refuse to be imposed by another user using ImpostorOption. The simple way is to do so , assume we have username named tom then `ImpostorOption.objects.get_or_create(user=User.objects.get(username='tom',allow=False)` will make tom unavailable to be imposed


###Using template tags
in your template add `{% load impostor_tags %}` then use :
 
  * `{% show_impostor_info %}` to show which user is being imposed
  * `{% show_hijack_warnig %}` to show a warning when user being hijacked
     need django-tracking2 and middleware has been added

##TODO/Wishlist
- None

##Known bugs
- None