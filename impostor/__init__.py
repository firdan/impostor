"""
impostor meant to hijack as another user, the following function will do
just that, be careful when use it.
"""
from django.contrib.auth import login, logout, get_user_model, \
                                BACKEND_SESSION_KEY, SESSION_KEY
from impostor.exceptions import AuthBackendMismatch, AuthBackendNotDetected
from impostor.backend import AuthBackend, is_allowed
from django.db import models
from impostor.models import ImpostorLog, ImpostorOption
from impostor.utils import logit
import datetime

# save current user id
IMPOSTOR_SESSION_KEY = '_impostor_user_id'
# to find which user currently hijack current user loged in
IMPOSTOR_HIJACK_KEY = '_impostor_hijack_id'
IMPOSTOR_TOKEN = '_impostor_token'

def stop_imposting(request):
    """ 
    change back to impostor user by changing its session and request.user 
    if impostor user does not exist, let it go ;) 
    """
    impostor_pk = request.session.get(IMPOSTOR_SESSION_KEY)

    try :
        impostor = get_user_model().objects.get(pk=impostor_pk)
        request.user = impostor
        request.session[SESSION_KEY] = impostor.pk
    except get_user_model().DoesNotExist:
        pass

    # remove the key no matter what
    request.session.pop(IMPOSTOR_SESSION_KEY, None)
    token = request.session.pop(IMPOSTOR_TOKEN, None)

    if token :
        try :
            impostorlog = ImpostorLog.objects.get(token=token)
            impostorlog.logged_out = datetime.datetime.now()
            impostorlog.save()
        except ImpostorLog.DoesNotExist :
            pass


def imposting(request, user):
    """
    save impostor session, this way when user want to stop imposting,
    user can easily switch to his current self.
    """
    # remove previous imposting
    stop_imposting(request)
    # make sure cannot pretend as anonymous (user can logout, why the need to
    # pretend? ) based on strange id (usually below 1)
    if user.is_anonymous() or user.pk < 1:
        return

    # has user allowed to be hijacked ?
    option, created = ImpostorOption.objects.get_or_create(user=user)

    # check impostor is from allowed group or superuser and
    # hijack user allow to be hijacked and not superuser
    if (is_allowed(request.user) or request.user.is_superuser) \
    and not user.is_superuser and option.allow:
        try :
            # make sure user has authenticated
            request.session[BACKEND_SESSION_KEY]

            impostor = request.user
            # switch the user and store impostor key
            request.session[IMPOSTOR_SESSION_KEY] = impostor.pk

            # hijack user
            request.user = user
            request.session[SESSION_KEY] = user.pk

            # log activities
            # logit(impostor, user, request=request, session_token=IMPOSTOR_TOKEN)
        except KeyError:
            raise AuthBackendNotDetected
